package weather;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDateTime;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("Let's check current weather!");

        ApplicationContext context = new AnnotationConfigApplicationContext("weather");
                //new ClassPathXmlApplicationContext("classpath:context.xml");

        ReportCollector collector = context.getBean(ReportCollector.class);
        ReportCollector collector2 = (ReportCollector) context.getBean("reportCollector");
                /*new SimpleReportCollector();
        ((SimpleReportCollector)collector).setTemperatureSensor(new CelciusTemperatureSensor());
        ((SimpleReportCollector)collector).setPressureSensor(new HectoPascalPressureSensor());
*/
        WeatherReport report = collector.collect(LocalDateTime.now().plusDays(0));
        System.out.println("Current weather report at [" + report.getTimestamp() + "]" );
        System.out.println("temperature [" + report.getTemperature() + "]");
        System.out.println("pressure [" + report.getPressure() + "]");
        
        List<TemperatureSensor> sensors = (List<TemperatureSensor>) context.getBean("temperatureSensors");
        System.out.println("sensors = " + sensors);

        System.out.println("done.");
    }
}
