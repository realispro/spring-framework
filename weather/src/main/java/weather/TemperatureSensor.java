package weather;

public interface TemperatureSensor {

    int getTemperature();
}
