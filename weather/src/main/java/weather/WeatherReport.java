package weather;

import lombok.*;

import java.time.LocalDateTime;

@RequiredArgsConstructor
/*@Getter
@Setter
@ToString
@EqualsAndHashCode*/
@Data
public class WeatherReport {

    private final LocalDateTime timestamp;

    private float temperature;
    private int pressure;

    public WeatherReport withTemperature(float temperature){
        this.temperature = temperature;
        return this;
    }

    public WeatherReport withPressure(int pressure){
        this.pressure = pressure;
        return this;
    }

}
