package weather.sensors;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import weather.TemperatureSensor;

import java.util.Random;

@Component
@Primary
@Qualifier("fahrenheit")
public class FahrenheitTemperatureSensor implements TemperatureSensor {
    @Override
    public int getTemperature() {
        System.out.println("generating random temp in fahrenheit");
        // -40 : 104
        return new Random(System.currentTimeMillis()).nextInt(144) - 40;
    }
}
