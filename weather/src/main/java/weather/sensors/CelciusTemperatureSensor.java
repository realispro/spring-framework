package weather.sensors;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import weather.TemperatureSensor;
import weather.config.Celcius;

import java.util.Random;

@Component
@Celcius
public class CelciusTemperatureSensor implements TemperatureSensor {
    @Override
    public int getTemperature() {
        System.out.println("generating random temp in celcius");
        // -40 : 40
        return new Random(System.currentTimeMillis()).nextInt(80) - 40;
    }
}
