package weather.sensors;

import org.springframework.stereotype.Component;
import weather.PressureSensor;

import java.util.Random;

@Component("pressureSensor")
public class HectoPascalPressureSensor implements PressureSensor {
    @Override
    public int getPressure() {
        // 950 : 1050
        return new Random(System.currentTimeMillis()).nextInt(100) + 950;
    }
}
