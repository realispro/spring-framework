package weather;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import weather.config.Celcius;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service("reportCollector")
@Lazy
@Scope("prototype")
@Getter
public class SimpleReportCollector implements ReportCollector {

    private final TemperatureSensor temperatureSensor;
    private final PressureSensor pressureSensor;

    private List<String> services = new ArrayList<>();

/*    public SimpleReportCollector(){
        System.out.println("simple report collector created.");
    }*/

    //@Autowired
    public SimpleReportCollector(
            @Celcius TemperatureSensor temperatureSensor,
            PressureSensor pressureSensor ){
        this.temperatureSensor = temperatureSensor;
        this.pressureSensor = pressureSensor;
        System.out.println("simple report collector created with params.");
    }

    @PostConstruct
    public void init(){
        System.out.println("report collector successfully created.");
    }


    @Override
    public WeatherReport collect(LocalDateTime dateTime){

        System.out.println("collecting weather data at "
                + dateTime.format(DateTimeFormatter.ISO_DATE_TIME));
        System.out.println("collector registered at services " + services);

        return new WeatherReport(dateTime)
                .withTemperature(temperatureSensor.getTemperature())
                .withPressure(pressureSensor.getPressure());
    }


    //@Autowired
    @Resource
    public void setServices(List<String> services) {
        this.services = services;
    }
}
