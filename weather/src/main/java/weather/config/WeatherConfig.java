package weather.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import weather.TemperatureSensor;
import weather.sensors.FahrenheitTemperatureSensor;

import java.util.List;

@Configuration
@PropertySource("classpath:weather.properties")
@EnableAspectJAutoProxy
public class WeatherConfig {

   /* private TemperatureSensor temperatureSensor;

    public WeatherConfig(TemperatureSensor temperatureSensor) {
        this.temperatureSensor = temperatureSensor;
    }*/

    @Bean("services")
    List<String> services(@Value("${meteo.service:unknown}") String meteoService){
        return List.of(
                "meteo.pl",
                "accuweather.com",
                //environment.getProperty("meteo.service")
                meteoService
        );
    }

    @Bean
    List<TemperatureSensor> temperatureSensors(
            TemperatureSensor temperatureSensor,
            @Celcius TemperatureSensor temperatureSensor2
            ){
        return List.of(
                temperatureSensor,
                temperatureSensor2
        );
    }

}
