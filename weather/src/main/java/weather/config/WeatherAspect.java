package weather.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Aspect
public class WeatherAspect {

    @Pointcut("execution(public * *(..))")
    public void everything(){}

    @Before("everything()")
    public void logEntering(JoinPoint jp){
        System.out.println("[ENTERING " + jp.toShortString() + " " + jp.getTarget().getClass().getSimpleName() + "]");
    }

    @After("everything()")
    public void logExiting(JoinPoint jp){
        System.out.println("[EXITING " + jp.toShortString() + " " + jp.getTarget().getClass().getSimpleName() + "]");
    }


    @Around("execution(public * *(java.time.LocalDateTime))")
    public Object verifyDate(ProceedingJoinPoint jp) throws Throwable {

        LocalDateTime dateTime = (LocalDateTime) jp.getArgs()[0];
        if (dateTime.isAfter(LocalDateTime.now())){
            throw new RuntimeException("future reports not available");
        }

        Object o = jp.proceed(jp.getArgs());
        System.out.println("[AROUND " + jp.toShortString() + " " + jp.getTarget().getClass().getSimpleName() + "]");

        return o;

    }


}
