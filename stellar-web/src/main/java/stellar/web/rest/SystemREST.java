package stellar.web.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;
import stellar.web.SystemValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/webapi")
@Slf4j
public class SystemREST {

    //private static final Logger log = LoggerFactory.getLogger(SystemREST.class);

    private final StellarService service;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;



    @GetMapping("/systems") // /systems?phrase=abc
    List<PlanetarySystem> getSystems(
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "foo", required = false) String foo,
            @RequestHeader Map<String, String> headers,
            @CookieValue(value = "Cookie_1", required = false) String cookie1
    ){

        log.info("cookie1:" + cookie1);
        log.info("foo header: " + foo);
        headers.keySet().stream()
                .filter(k->k.startsWith("foo-"))
                .forEach(k->log.info( k + ":" + headers.get(k)));

        return phrase==null
                ? service.getSystems()
                : service.getSystemsByName(phrase);
    }

    @GetMapping("/systems/{id}")
    //@Secured("ROLE_ADMIN")
    ResponseEntity<PlanetarySystem> getSystem(@PathVariable("id") int id){
        PlanetarySystem system = service.getSystemById(id);
        if(system==null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(system);
        }
    }

    @PostMapping("/systems")
    ResponseEntity<?> addSystem(
            HttpServletRequest request,
            @RequestBody @Validated PlanetarySystem system,
            Errors errors){
        log.info("about to add system {}", system);

        boolean valid = !errors.hasErrors();

        if(valid) {
            system = service.addPlanetarySystem(system);
            return ResponseEntity.status(HttpStatus.CREATED).body(system);
        } else {
            StringBuilder sb = new StringBuilder("errors:\n");
            errors.getAllErrors().forEach(e->{
                String code = e.getCode();
                Locale locale = localeResolver.resolveLocale(request);
                //new Locale("fr", "FR");
                // messages_fr_FR
                // messages_fr
                // messages_<OS_lang>
                // messages
                String msg = messageSource.getMessage(code, e.getArguments(), locale);
                sb.append(msg).append("\n");
            });

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(sb.toString());
        }
    }



}
