package stellar.web.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class PlanetREST {

    private final StellarService service;

    @GetMapping("/planets")
    List<Planet> getPlanets() {
        return service.getPlanets();
    }

    @GetMapping("/planets/{id}")
    ResponseEntity<Planet> getPlanet(@PathVariable("id") int id) {

        if(id<0){
            throw new IllegalArgumentException("negative id");
        }

        Planet planet = service.getPlanetById(id);
        if (planet == null) {
            return ResponseEntity.notFound().build();
        }else {
            return ResponseEntity.ok(planet);
        }
    }

    @GetMapping("/systems/{systemId}/planets")
    ResponseEntity<List<Planet>> getPlanetsBySystemId(@PathVariable("systemId") int systemId) {
        PlanetarySystem system = service.getSystemById(systemId);

        if (system!=null) {
            List<Planet> planets = service.getPlanets(system);
            return ResponseEntity.ok(planets);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
