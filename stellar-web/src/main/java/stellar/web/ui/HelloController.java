package stellar.web.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HelloController {

    @GetMapping("/hello")
    String sayHello(Model model){
        model.addAttribute("hello_text", "Hey Universe!");
        return "hello";
    }
}
