package stellar.web.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
public class PlanetController {

    private final StellarService service;

    @GetMapping("/planets")
    String getPlanetsBySystem(Model model, @RequestParam(value = "systemId") int systemId){
        log.info("fetching planets by system {}", systemId);

        PlanetarySystem system = service.getSystemById(systemId);

        if (system != null) {
            List<Planet> planets = service.getPlanets(system);
            model.addAttribute("system", system);
            model.addAttribute("planets", planets);
            return "planets";
        } else {
            log.warn("system {} not found", systemId);
            return "redirect:/systems";
        }

    }
}
