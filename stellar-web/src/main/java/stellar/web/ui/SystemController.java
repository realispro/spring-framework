package stellar.web.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
public class SystemController {

    private final StellarService service;

    @GetMapping("/systems")
    String getSystems(Model model, @RequestParam(value = "phrase", required = false) String phrase){
        log.info("fetching systems");

        List<PlanetarySystem> systems = phrase==null
                        ? service.getSystems()
                        : service.getSystemsByName(phrase);

        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("/addSystem")
    String addSystemPrepare(Model model){

        PlanetarySystem system = new PlanetarySystem();
        system.setName("Unknown");
        system.setDistance(987);

        model.addAttribute("systemForm", system);

        return "addSystem";
    }

    @PostMapping("/addSystem")
    String addSystem(@ModelAttribute("systemForm") @Validated PlanetarySystem system, Errors errors){
        log.info("adding new system {}", system);

        if(errors.hasErrors()){
            return "addSystem";
        } else {
            service.addPlanetarySystem(system);
            return "redirect:/systems";
        }
    }

}
