package stellar.web.config;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import stellar.web.SystemValidator;

@ControllerAdvice
@RequiredArgsConstructor
public class StellarAdvice {

    private final SystemValidator validator;

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<String> handleException(IllegalArgumentException e){
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(e.getMessage());
    }

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }
}
