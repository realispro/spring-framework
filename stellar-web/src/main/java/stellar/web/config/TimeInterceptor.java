package stellar.web.config;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.time.LocalTime;

@Component
@Setter
@Slf4j
public class TimeInterceptor implements HandlerInterceptor {

    private int opening = 9;
    private int closing = 17;

    @Override
    public boolean preHandle(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object handler) throws Exception {

        int hour = LocalTime.now().getHour();
        if(opening>hour || closing<=hour){
            log.warn("outside opening hours");
            response.setStatus(418);
            return false;
        } else {
            log.info("service open");
            return true;
        }
    }
}
