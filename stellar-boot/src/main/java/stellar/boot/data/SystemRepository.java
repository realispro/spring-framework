package stellar.boot.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.boot.model.PlanetarySystem;

public interface SystemRepository extends JpaRepository<PlanetarySystem, Integer> {
}
