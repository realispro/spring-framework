package stellar.boot.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.boot.model.Planet;

public interface PlanetRepository extends JpaRepository<Planet, Integer> {
}
