package stellar.boot.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.boot.data.SystemRepository;
import stellar.boot.model.PlanetarySystem;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class SystemREST {

    private final SystemRepository repository;
    private final SystemValidator validator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @GetMapping("/systems")
    List<PlanetarySystem> getSystems(){
        log.info("fetching systems");
        return repository.findAll();
    }

    @PostMapping("/systems")
    ResponseEntity<PlanetarySystem> addSystem(@RequestBody @Validated PlanetarySystem system, Errors errors){

        log.info("adding system {}", system);

        if(errors.hasErrors()){
            return ResponseEntity.badRequest().build();
        } else {
            system = repository.save(system);
            return ResponseEntity.status(HttpStatus.CREATED).body(system);
        }
    }

}
