package stellar.boot.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Planet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int size;
    private int weight;
    private int moons;
    @ManyToOne
    private PlanetarySystem system;
}
