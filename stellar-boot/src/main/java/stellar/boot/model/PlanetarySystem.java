package stellar.boot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "planetarysystem")
public class PlanetarySystem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String star;
    private Date discovery;
    private float distance;
    @JsonIgnore
    @OneToMany(mappedBy = "system")
    private List<Planet> planets;
}
