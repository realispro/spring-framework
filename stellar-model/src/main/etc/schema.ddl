create table planetarysystem
(
    id        serial primary key,
    name      varchar(255),
    star      varchar(255),
    distance  float,
    discovery date
);
create table planet
(
    id        serial primary key,
    name      varchar(255),
    size      int,
    weight    int,
    moons     int,
    system_id int
);
insert into planetarysystem (id, name, star, distance, discovery)
values (1, 'Solar System', 'Sun', 1.11, '1212-02-13 00:00:00');
insert into planetarysystem (id, name, star, distance, discovery)
values (2, 'Kepler-90 System', 'Kepler-90', 2.567, '2012-06-23 00:00:00');
insert into planetarysystem (id, name, star, distance, discovery)
values (3, 'Trappist-1 System', 'Trappist-1', 13.121, '2016-01-12 00:00:00');
insert into planetarysystem (id, name, star, distance, discovery)
values (4, 'Kepler-11 System', 'Kepler-11', 7.61, '2011-02-13 00:00:00');
insert into planetarysystem (id, name, star, distance, discovery)
values (5, 'Kepler-12 System', 'Kepler-12', 7.62, '2014-02-13 00:00:00');
insert into planetarysystem (id, name, star, distance, discovery)
values (6, 'Kepler-13 System', 'Kepler-12', 7.64, '2018-02-13 00:00:00');
insert into planet (id, name, size, weight, moons, system_id)
values (1, 'Mercury', 2439, 3, 0, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (12, 'Venus', 6051, 5, 0, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (13, 'Earth', 6371, 60, 1, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (14, 'Mars', 3389, 6, 2, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (15, 'Jupiter', 69911, 10000, 79, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (16, 'Neptun', 24622, 1000, 14, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (17, 'Saturn', 58232, 5000, 82, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (18, 'Uranus', 25362, 800, 27, 1);
insert into planet (id, name, size, weight, moons, system_id)
values (19, 'Pluto', 102, 2, 0, 1);