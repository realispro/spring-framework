package stellar.service;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import stellar.dao.impl.mem.InMemoryPlanetDAO;
import stellar.dao.impl.mem.InMemorySystemDAO;
import stellar.model.Planet;
import stellar.service.impl.StellarServiceImpl;

import java.util.List;

public class StellarServiceStart {

    public static void main(String[] args) {
        System.out.println("Let's explore!");

        ApplicationContext context = new AnnotationConfigApplicationContext("stellar");

        StellarService service = context.getBean(StellarService.class);
                /*new StellarServiceImpl();
        ((StellarServiceImpl)service).setSystemDAO(new InMemorySystemDAO());
        ((StellarServiceImpl)service).setPlanetDAO(new InMemoryPlanetDAO());*/

        List<Planet> planets = service.getPlanets(service.getSystemById(1));

        planets.forEach(p-> System.out.println(p));

    }
}
