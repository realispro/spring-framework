/*
package stellar.dao.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Primary
public class JPAPlanetDAO implements PlanetDAO {

    @PersistenceContext(unitName = "stellar")
    private EntityManager em;

    @Override
    public List<Planet> getAllPlanets() {
        return em.createQuery("select p from Planet p").getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        List<Planet> planets = em.createQuery("select p from Planet p where p.system = :system")
                .setParameter("system", system)
                .getResultList();
        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return em.find(Planet.class, id);
    }

    @Override
    @Transactional
    public Planet addPlanet(Planet p) {
        em.persist(p);
        return p;
    }
}
*/
