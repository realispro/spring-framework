package stellar.dao.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface SystemRepository extends JpaRepository<PlanetarySystem, Integer> {

    //@Query(value = "select s from PlanetarySystem s where s.name like :name", nativeQuery = false)
    List<PlanetarySystem> findAllByNameContaining(String name);

}
