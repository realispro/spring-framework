package stellar.dao.data;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;

@Repository
@Primary
@RequiredArgsConstructor
public class SystemDAOAdapter implements SystemDAO {

    private final SystemRepository repository;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return repository.findAll();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return repository.findAllByNameContaining(like);
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        return repository.save(system);
    }
}
