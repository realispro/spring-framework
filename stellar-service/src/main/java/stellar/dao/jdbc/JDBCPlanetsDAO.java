package stellar.dao.jdbc;


import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.dao.SystemDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class JDBCPlanetsDAO implements PlanetDAO {

    public static final Logger logger = Logger.getLogger(JDBCPlanetsDAO.class.getName());

    public static final String SELECT_ALL_PLANETS = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons, p.system_id as system_id" +
            "from planet p";

    public static final String SELECT_PLANETS_BY_SYSTEM = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons, p.system_id as system_id " +
            "from planet p where system_id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM_AND_NAME = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons, p.system_id as system_id " +
            "from planet p where system_id=? and name like ?";

    public static final String SELECT_PLANET_BY_ID = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons, p.system_id as system_id " +
            "from planet p where p.id=?";






    private final JdbcTemplate jdbcTemplate;
    private final SystemDAO systemDAO;

    public JDBCPlanetsDAO(DataSource dataSource, SystemDAO systemDAO) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.systemDAO = systemDAO;
    }
    @Override
    public List<Planet> getAllPlanets() {
        List<Planet> planets =
                jdbcTemplate.query(
                        SELECT_ALL_PLANETS,
                        new PlanetMapper());
        planets.forEach(p->p.setSystem(systemDAO.getPlanetarySystem(p.getSystem().getId())));
        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {

        List<Planet> planets =
                jdbcTemplate.query(SELECT_PLANETS_BY_SYSTEM, new PlanetMapper(), system.getId());
        planets.forEach(p->p.setSystem(system));

        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        try {
            Planet p = jdbcTemplate.queryForObject(SELECT_PLANET_BY_ID, new PlanetMapper(), id);
            p.setSystem(systemDAO.getPlanetarySystem(p.getSystem().getId()));
            return p;
        }catch (EmptyResultDataAccessException e){
            logger.severe("empty result for planet query");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }


    private Planet mapPlanet(ResultSet rs) throws SQLException {
        Planet p = new Planet();
        p.setId(rs.getInt("planet_id"));
        p.setName(rs.getString("planet_name"));
        p.setSize(rs.getInt("planet_size"));
        p.setWeight(rs.getInt("planet_weight"));
        p.setMoons(rs.getInt("planet_moons"));
        return p;
    }
}