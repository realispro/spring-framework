package stellar.dao.jdbc;

import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class JDBCSystemsDAO implements SystemDAO {

    public static final Logger logger = Logger.getLogger(JDBCSystemsDAO.class.getName());

    public static final String SELECT_ALL_SYSTEMS = "select ps.id as system_id, " +
            "ps.name as system_name, ps.distance as system_distance, ps.discovery as system_discovery, ps.star as system_star " +
            "from planetarysystem ps";

    public static final String SELECT_SYSTEMS_BY_NAME = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance, ps.star as system_star  " +
            "from planetarysystem ps where name like ?";

    public static final String SELECT_SYSTEM_BY_ID = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance, ps.star as system_star  " +
            "from planetarysystem ps where id=?";

    private final JdbcTemplate jdbcTemplate;

    public JDBCSystemsDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        List<PlanetarySystem> systems =
                jdbcTemplate.query(
                        SELECT_ALL_SYSTEMS,
                        new SystemMapper());
                /*new ArrayList<>();

        try(Connection connection = dataSource.getConnection();Statement stmt = connection.createStatement();){
            ResultSet rs = stmt.executeQuery(SELECT_ALL_SYSTEMS);
            while(rs.next()){
                systems.add(mapSystem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        return systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        List<PlanetarySystem> systems =
                jdbcTemplate.query(SELECT_SYSTEMS_BY_NAME, new SystemMapper(), "%" + like + "%");

        return systems;
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {

        try {
            return jdbcTemplate.queryForObject(SELECT_SYSTEM_BY_ID, new SystemMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            logger.severe("empty result for system query");
            e.printStackTrace();
            return null;
        }

        /*try(Connection c = dataSource.getConnection(); PreparedStatement stmt = c.prepareStatement(SELECT_SYSTEM_BY_ID)){

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return mapSystem(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;*/
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {

        KeyHolder kh = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement stmt = con.prepareStatement(
                                "INSERT INTO PLANETARYSYSTEM(name, star, discovery, distance) VALUES (?,?,?,?)",
                                new String[]{"id"});
                        stmt.setString(1, system.getName());
                        stmt.setString(2, system.getStar());
                        stmt.setDate(3, new java.sql.Date(system.getDiscovery().getTime()));
                        stmt.setFloat(4, system.getDistance());
                        return stmt;
                    }
                },
                kh
        );

        int key = kh.getKey().intValue();
        system.setId(key);

        return system;
    }

    public PlanetarySystem mapSystem(ResultSet rs) throws SQLException {
        PlanetarySystem ps = new PlanetarySystem();
        ps.setId(rs.getInt("system_id"));
        ps.setName(rs.getString("system_name"));
        ps.setDistance(rs.getFloat("system_distance"));
        ps.setDiscovery(rs.getDate("system_discovery"));
        return ps;
    }

}
