package stellar.dao.jdbc;

import org.springframework.jdbc.core.RowMapper;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanetMapper implements RowMapper<Planet> {

    @Override
    public Planet mapRow(ResultSet rs, int rowNum) throws SQLException {
        Planet p = new Planet();
        p.setId(rs.getInt("planet_id"));
        p.setName(rs.getString("planet_name"));
        p.setSize(rs.getInt("planet_size"));
        p.setWeight(rs.getInt("planet_weight"));
        p.setMoons(rs.getInt("planet_moons"));
        PlanetarySystem system = new PlanetarySystem();
        system.setId(rs.getInt("system_id"));
        p.setSystem(system);
        return p;
    }
}
